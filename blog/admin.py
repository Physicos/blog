from django.contrib import admin
from blog.models import Post, Comment


class CommentInline(admin.TabularInline):
    model = Comment


class PostAdmin(admin.ModelAdmin):
    inlines = [
        CommentInline
    ]


class CommentAdmin(admin.ModelAdmin):
    list_display = ('author', 'content', 'post', 'created_on')
    list_filter = ['created_on']
    search_fields = ('author', 'content')


admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
